from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User
from django.db import transaction
import sqlite3
from .models import Account
from .models import Pii
from .models import Transfer

# Create your views here.

from django.db import connection

def addTransaction(sender, receiver, amount, message):
	print("writing to logbook")
	with connection.cursor() as cursor:
		query = f'INSERT INTO pages_transfer (receiver_id,sender_id,amount,message) VALUES ({receiver},{sender},{amount},"{message}")'
		print(query)
		cursor.execute(query)
		
@login_required 
@transaction.atomic
def transferView(request):
	if request.method == 'POST':
		sender = request.POST.get('from')
		receiver = request.POST.get('to')
		amount = int(request.POST.get('amount'))
		message = request.POST.get('message')

		print(request.POST)
		acc1 = Account.objects.get(iban=sender)
		#acc1 = Account.objects.filter(owner_id=request.user.id)[0]
		acc2 = Account.objects.get(iban=receiver)

		# Check whether there is balance
		# Check for negative
		if amount > 0 and acc1.balance >= amount and acc1 != acc2:
			print("tries something")
			acc1.balance -= amount
			acc2.balance += amount

			acc1.save()
			acc2.save()
			addTransaction(acc1.owner_id, acc2.owner_id, amount, message)

	return redirect('/')

def getTransfers(id):
	transfers = Transfer.objects.all#filter(Q(sender_id=id) | Q(receiver_id=id))
	for transfer in transfers:
		print(f'{transfer}=')

	return transfers


@login_required
def transactionsView(request):
	id = request.user.id

	sent = Transfer.objects.filter(sender_id=id)
	received = Transfer.objects.filter(receiver_id=id)

	context = {'sent': sent, 'received': received}
	return render(request, 'pages/transactions.html', context)

@login_required
def piiView(request):
	id = request.user.id

	pii = Pii.objects.filter(owner_id=id)[0]
	context = {'pii': pii}
	return render(request, 'pages/pii.html', context)


@login_required
def homePageView(request):
	if request.method == 'POST':
		sender = request.POST.get('from')
		receiver = request.POST.get('to')
		amount = int(request.POST.get('amount'))
		transfer(sender, receiver, amount)

	id = request.user.id
	accounts = Account.objects.exclude(owner_id=id)
	own_account = Account.objects.filter(owner_id=id)[0]
	context = {'accounts': accounts, 'own_account': own_account}
	return render(request, 'pages/index.html', context)
