from django.db import models

# Create your models here.

from django.contrib.auth.models import User

class Account(models.Model):
	owner = models.ForeignKey(User, on_delete=models.CASCADE)
	iban = models.TextField()
	balance = models.IntegerField()

	def __str__(self):
		return f'{self.owner=} {self.iban=} {self.balance=}'


class Pii(models.Model):
	owner = models.ForeignKey(User, on_delete=models.CASCADE)
	address = models.TextField()
	phone = models.TextField()
	social_sec = models.TextField()

class Transfer(models.Model):
	#sender = models.ForeignKey('Sender', on_delete=models.CASCADE)
	#receiver = models.ForeignKey('Receiver', on_delete=models.CASCADE)
	sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name='sent_money')
	receiver = models.ForeignKey(User, on_delete=models.CASCADE, related_name='received_money')

	amount = models.IntegerField()
	message = models.TextField()

	def __str__(self):
		return '%s -> %s: %s (%s)' % (self.sender, self.receiver, self.amount, self.message)
