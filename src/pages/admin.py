from django.contrib import admin

# Register your models here.

from .models import Account
from .models import Pii
from .models import Transfer


admin.site.register(Account)
admin.site.register(Pii)
admin.site.register(Transfer)