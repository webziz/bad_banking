from django.apps import AppConfig
AppConfig.default = False

class PagesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'pages'
