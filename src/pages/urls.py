from django.urls import path

from .views import homePageView, transferView, transactionsView, piiView

urlpatterns = [
    path('', homePageView, name='home'),
    path('transfer/', transferView, name='transfer'),
    path('transactions/', transactionsView, name='transactions'),
    path('pii/', piiView, name='pii')
]
