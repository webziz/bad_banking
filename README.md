# Cyber Security Base 2021 - Bad Banking

Project 1 for the Cyber Security Base 2021 [course](https://cybersecuritybase.mooc.fi/module-3.1).

A super simple "banking application" with 5 of flaws from [OWASP top 10 list](https://owasp.org/www-project-top-ten/) intentionally added.

## Functionality
You can transfer money from user to another, list transactions

### Users

You can use the following users:
- joe:arbuckle
- linda:dabadabaduu

## Faults 
Numbered based on their standing in the OWASP list (5 in total)

### [1. Injection](https://owasp.org/www-project-top-ten/2017/A1_2017-Injection)
Sending money from one user to another is dreadfully implemented, especially in the "message" part of the transaction. The SQL query is easily escaped.

### [2. Broken authentication](https://owasp.org/www-project-top-ten/2017/A2_2017-Broken_Authentication) 
The App utilizes Django's built-in user management. This does not bring any tools to manage log in throttling or anything, so it's really brute forceable as there is nothing to prevent it.

### [3. Sensitive data exposure](https://owasp.org/www-project-top-ten/2017/)
In the database there is PII data (social security numbers) stored in cleartext. This itself is bad, but this data is accessible by the SQL injection flaw.  

### [5. Broken access control](https://owasp.org/www-project-top-ten/2017/A5_2017-Broken_Access_Control) 
Using the UI one is limited to moving data from own account to another people's accounts, but these bounds can be escaped via CURL (or equivalent) as there is no check constraining actions moving money from other's accounts. 

### [10. Insufficient Logging & Monitoring](https://owasp.org/www-project-top-ten/2017/A10_2017-Insufficient_Logging%2526Monitoring)
There is no logging or monitoring whatsoever. Everything bad happening to the system will happen, with nothing stopping it. 